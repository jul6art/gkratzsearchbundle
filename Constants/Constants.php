<?php

namespace Gkratz\SearchBundle\Constants;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 19/03/2017
 * Time: 21:45
 */
class Constants
{
    const LIMIT_SIZE_SEARCH_RESULTS_STRING = 500;
    const ITEM_PER_PAGE = 25;
}