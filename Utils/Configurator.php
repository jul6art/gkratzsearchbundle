<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 15-03-17
 * Time: 13:10
 */

namespace Gkratz\SearchBundle\Utils;


class Configurator
{
    private $search;
    private $notification;

    /**
     * @param $search
     */
    public function setSearch($search){
        $this->search = $search;
    }

    /**
     * @return mixed
     */
    public function getSearch(){
        return $this->search;
    }

    /**
     * @param array $searchConfig
     * @return array
     */
    public function convertSearchArray(array $searchConfig){
        $convertedArray = array();
        $fields = explode(',', $searchConfig['fields']);

        foreach ($fields as $field){
            $field = trim($field);
            $cols = explode('.', $field);
            $convertedArray[$cols[0]] []= $cols[1];
        }

        $result = array(
            'allow_approaching' => $searchConfig['allow_approaching'],
            'results' => $convertedArray
        );

        return $result;
    }
}