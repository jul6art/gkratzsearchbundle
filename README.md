## INSTALLATION

![](https://bitbucket.org/jul6art/gkratzsearchbundle/raw/master/Resources/public/assets/screenshot.png)
![](https://bitbucket.org/jul6art/gkratzsearchbundle/raw/master/Resources/public/assets/screenshot2.png)

1) //-- add in the "composer.json" of your project --//

    "require": {
        // ...
        "gkratz/searchbundle": "dev-master"
    }


2) //-- activate the bundle in your "app\AppKernel.php" --//

    new Gkratz\SearchBundle\GkratzSearchBundle(),
    new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),


3) //-- enter this command in your terminal and don't worry about the "Undefined index: search" alert when installing the vendors. Error will be corrected by other steps of the installation --//

    php composer update gkratz/searchbundle


4) //-- update your "app\config\config.yml" file --//

    # KNPPaginatorBundle Configuration
    knp_paginator:
        default_options:
            distinct:             false
        template:
            # pagination:           YourCustomBundle:pagination.html.twig
    
    # GkratzSearchBundle Configuration
        gkratz_search:
            search:
                allow_approaching:    true                      # may be very slow. set false to only allow perfect match.
                fields:               Test.title, Test.content  # entity must exists if you let it!!!!!!!!!!!!!!!!!!!!!!!


5) //-- make sure that this line in "app\config\config.yml" is decommented to enable translations --//

    framework:
        translator:      { fallbacks: ["%locale%"] }


6) //-- update your "app\config\routing.yml" file --//

    #this route must ABSOLUTELY be
    #at the top of the config.yml file
    #LINE 1
    GkratzSearch:
        resource: "@GkratzSearchBundle/Controller/"
        type:     annotation
        prefix:   /search


7) //-- generate search entity THAT EXTENDS Search MODEL  --//

    php bin/console doctrine:generate:entity

//--   shortcut name: AppBundle:Search --//

    <?php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Gkratz\SearchBundle\Model\Search as BaseSearch;

    /**
     *
     *  @ORM\Entity(repositoryClass="AppBundle\Repository\SearchRepository")
     *  @ORM\Table(name="search")
     */
    class Search extends BaseSearch
    {
        /**
         * @var int
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
         * Get id
         *
         * @return int
         */
        public function getId()
        {
            return $this->id;
        }
    }


8) //-- update your "main layout" --//

    <form action="{{ path('gkratz_search_search_search') }}" method="post">
        {{ render(controller('GkratzSearchBundle:Search:index', {request: app.request})) }}
    </form>


9) //-- enter these commands in your terminal --//

    php bin/console doctrine:generate:entities App
    php bin/console doctrine:schema:update --force


## HOW TO USE

    - you can change limit results per page and maximum size of search results in Gkartz\AdminBundle\Constants\Constants.php
        
    - to use the search plugin, you need to activate the fields for the search in the config.yml and
        # config.yml
        # GkratzAdminBundle Configuration
        gkratz_admin:
            search:
                allow_approaching:    true                    # may be very slow. set false to only allow perfect match.
                fields:               Post.title, Post.content, User.username, User.lastname, User.firstname, User.locality, User.country
            # ...


## OVERRIDE

    - you can easily overwrite translations, controllers and views. The bundle is KISS made, simple and powerful.
    - to overwrite results views, make a GkratzSearchBundle/views/index.html.twig file in app/Resources -> ie: 
        {% extends 'base.html.twig' %}
        
        {% block body %}
            <div>
                <h1>{{ 'Search results' | trans }} - {{ entities.getTotalItemCount }}</h1>
        
                <h2>{{ entities.0 is defined ? entities.0.searchText | capitalize : '' }}</h2>
        
                <div>
                    {% if approach is defined %}
                        <a href="{{ path('gkratz_search_search_search', {approaching: 1, search: app.request.get('id')}) }}"> {{ 'Expand search' | trans }}</a>
                    {% endif %}
                </div>
        
                <section class="row">
                    {% for entity in entities %}
                        <a href="yourcustomlinkhere">
                            <h2>#{{ loop.index }} {{ entity.class | capitalize }} id {{ entity.elementId }}</h2>
                            <div>
                                {{ entity.resultText | searchtext(entity.searchText, allowApproaching) }}
                            </div>
                        </a>
                    {% else %}
                        <p class="error">{{ "No entities" | trans }}</p>
                    {% endfor %}
                </section>
        
                <div class="navigation">
                    {{ knp_pagination_render(entities) }}
                </div>
            </div>
        {% endblock %}
        
    - you can overwrite controllers to manage results with user roles or domain (ie: different engine on admin or front)
