<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 15-03-17
 * Time: 14:27
 */

namespace Gkratz\SearchBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Search
 * @package Gkratz\SearchBundle\Model
 */
abstract class Search implements SearchInterface
{
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="search_id", type="string", length=255)
     */
    protected $searchId;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var string
     * @ORM\Column(name="class", type="string", length=255)
     */
    protected $class;

    /**
     * @var integer
     * @ORM\Column(name="element_id", type="integer")
     */
    protected $elementId;

    /**
     * @var string
     * @ORM\Column(name="search_text", type="text")
     */
    protected $searchText;

    /**
     * @var string
     * @ORM\Column(name="result_text", type="text")
     */
    protected $resultText;

    /**
     * @var integer
     * @ORM\Column(name="points", type="integer")
     */
    protected $points;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return int
     */
    public function getElementId()
    {
        return $this->elementId;
    }

    /**
     * @param int $elementId
     */
    public function setElementId($elementId)
    {
        $this->elementId = $elementId;
    }

    /**
     * @return string
     */
    public function getSearchText()
    {
        return $this->searchText;
    }

    /**
     * @param string $searchText
     */
    public function setSearchText($searchText)
    {
        $this->searchText = $searchText;
    }

    /**
     * @return string
     */
    public function getResultText()
    {
        return $this->resultText;
    }

    /**
     * @param string $resultText
     */
    public function setResultText($resultText)
    {
        $this->resultText = $resultText;
    }

    /**
     * @return string
     */
    public function getSearchId()
    {
        return $this->searchId;
    }

    /**
     * @param string $searchId
     */
    public function setSearchId($searchId)
    {
        $this->searchId = $searchId;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param int $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }
}